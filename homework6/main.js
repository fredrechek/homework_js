const someList = ["Some string", 12, 2.5, true, false, [], NaN, { hello: "world" }, null, undefined];
const newArray = [];

const filterBy = function (array, except) {
  for (let i = 0; i < array.length; i++) {
    let element = array[i];
    switch (except) {
      case "object":
        if (typeof element !== except) {
          newArray.push(element)
        }
        break;
      case "string":
        if (typeof element !== except) {
          newArray.push(element)
        }
        break;
      case "number":
        if (typeof element !== except) {
          newArray.push(element)
        }
        break;
      case undefined:
        if (element !== except) {
          newArray.push(element)
        }
        break;
      case null:
        if (element !== except) {
          newArray.push(element)
        }
        break;
    }
  }
  return newArray;
};

filterBy(someList, undefined);
console.log(newArray);