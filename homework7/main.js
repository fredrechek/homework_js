const list = ['hello', 'Ihor', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const link = document.createElement("ul");
document.body.appendChild(link);

function entrance(parentEl, array) {
  const newArray = array.map(item => {
    const li = document.createElement("li");
    li.textContent = item
    return li;
  })
  console.log(newArray)

  newArray.forEach(element => {
    parentEl.appendChild(element)
  });

}

entrance(document.querySelector("ul"), list)