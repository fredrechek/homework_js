const input = document.createElement("input");
input.setAttribute("placeholder", "price");
const span = document.createElement("span");


window.document.addEventListener("DOMContentLoaded", addInput);
function addInput() {
  document.body.prepend(input);
};

input.addEventListener("focus", function (event) {
  event.target.style.cssText =
    "border: 1px solid green; outline: 1px solid green;"
});

input.addEventListener("blur", function (event) {
  if (!document.body.contains(span)) {
    event.target.style.cssText =
      "border: 1px solid transparent; outline: 1px solid transparent;";
    console.log(event.target.value);
    if (event.target.value <= 0) {
      showError(event.target)
    } else {
      count(event.target);
    }
  }

})

function showError(element) {
  element.style.cssText = "border: 1px solid red; outline: 1px solid red;";
  const error = document.createElement("span");
  error.textContent = "Please enter correct price!";
  element.after(error)
  setTimeout(() => {
    error.remove();
    element.value = ""
    element.style.cssText = 'border: 1px solid black';
  }, 1000)

}

function count(element) {
  element.cssText = "color: green"
  span.innerHTML = `Текущая цена: ${element.value} <button> X </button>`
  element.before(span);
  removeSpan(span);
};



function removeSpan(element) {
  element.addEventListener("click", function (event) {
    if (event.target.tagName === "BUTTON") {
      event.target.parentElement.remove()
      input.value = " ";
    }
  })
}