
const tabs = document.querySelector('.tabs');
const posts = document.querySelectorAll(".tabs-post");

tabs.addEventListener("click", function (event) {
  if (event.target.tagName === "LI") {


    Array.from(this.children).forEach(element => {
      element.classList.remove("active")
    });
    event.target.classList.toggle('active');

    Array.from(posts).forEach(element => {
      element.classList.remove("active");
      if (element.dataset.id === event.target.dataset.id) {
        element.classList.add("active");
      }
    })

  }

})