const number1 = parseInt(prompt('Enter your number'));
const number2 = parseInt(prompt('Enter your number'));
const operator = prompt('Enter operator +, -, *, / ', '+');

const calc = function (num1, num2, act) {
  let result = 0;
  switch (act) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      result = num1 / num2;
      break;

    default:
      break;
  }
  return result;
};

console.log(calc(number1, number2, operator));
