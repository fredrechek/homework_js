const createNewUser = function (firstName = prompt("Enter your name", "Alex"), lastName = prompt("Enter your last name", "Firsov")) {

  const date = prompt("Дата рождения в формате dd.mm.yyyy", "07.06.1996").split(".").reverse();

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: date,
    getAge: function () {
      let now = new Date();
      const subspecies = new Date(date);
      return now.getFullYear() - subspecies.getFullYear();
    },
    getPassword: function () {
      return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday[0]);
    },
    getLogin: function () {

      return ((this.firstName[0] + this.lastName).toLowerCase())

    }
  }
  return newUser;
};

const result = createNewUser("Naruto", "Uzumaki");
console.log(result.getLogin());
console.log(result.getAge());
console.log(result.getPassword());